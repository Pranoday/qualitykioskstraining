package QualityKiosksTraining.FillingupHashmap;

/**
 * Hello world!
 *
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class App 
{
    public static void main( String[] args )
    {
    	HashMap[]arr=ReadDataFromExcel();
    	
    	for(int i=0;i<arr.length;i++)
    	{
    		System.out.println("Name "+arr[i].get("UserName"));
    		System.out.println("Password "+arr[i].get("Password"));
    		System.out.println("Error "+arr[i].get("Error"));
    	}	
    }



	private static XSSFWorkbook ExcelWBook;
	private static XSSFSheet ExcelWSheet;
	
	public static HashMap[] ReadDataFromExcel()
	{

		
		String DataFilePath="D:\\QualityKioskTraining\\TestCasesProject\\OrangeHRMTestCases\\DataFiles\\LoginTestData.xlsx";
		
		//HashMap<String,String>Data = new HashMap<String,String>();
		HashMap[]ArrData=null;
		FileInputStream ExcelFile;
		
		
		try 
		{
			ExcelFile = new FileInputStream(DataFilePath);
			ExcelWBook=new XSSFWorkbook(ExcelFile);
			ExcelWSheet=ExcelWBook.getSheet("Sheet1");
			
			int startRow = 1;
			int HasMapArrIndex=0;
			int startCol = 0;

			int totalRows = ExcelWSheet.getLastRowNum();
			int totalCols = 3;
			
			String ColumnData;
			
			ArrData=new HashMap[totalRows];
			
			for(int i=startRow;i<=totalRows;i++)
			{
				HashMap<String,String>Data = new HashMap<String,String>();
				for(int j=startCol;j<totalCols;j++)
				{
					//Data[i][j]=getCellData(i,j);
					ColumnData=getCellData(i,j);
					Data.put(getCellData(0,j), ColumnData);
						
					
				}
				ArrData[HasMapArrIndex++]=Data;
				
			}	
			
		
		
		} catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("Please check if file at "+DataFilePath+" exists or not");
		} catch (IOException e) {
			
			// TODO Auto-generated catch block
			System.out.println("Problem in reading file "+DataFilePath);
		}
		
		return ArrData;
	}

	public static String getCellData(int Row,int Col)
	{
		//Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
		XSSFCell Cell =ExcelWSheet.getRow(Row).getCell(Col);
		String CellData=Cell.getStringCellValue();
		return CellData;
	}

}
