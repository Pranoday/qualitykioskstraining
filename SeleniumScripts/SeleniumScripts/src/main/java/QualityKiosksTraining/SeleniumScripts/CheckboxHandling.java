package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckboxHandling {

	public static void main(String[] args) throws InterruptedException 
	{
	
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.manage().window().maximize();
		D.get("https://letskodeit.teachable.com/p/practice");

		WebElement benzcheck=D.findElementById("benzcheck");
		//D.findElementById("benzcheck").click();
		benzcheck.click();
		boolean IsSelectedOrNot=benzcheck.isSelected();
		
		if(IsSelectedOrNot==true)
			System.out.println("Clicking on checkbox selected it...PASSED");
		else
			System.out.println("Clicking on checkbox did not selected it...FAILED");
		Thread.sleep(5000);
		benzcheck.click();
		IsSelectedOrNot=benzcheck.isSelected();
		if(IsSelectedOrNot==false)
			System.out.println("Clicking on checkbox again Deselected it...PASSED");
		else
			System.out.println("Clicking on checkbox again did not Deselected it...FAILED");
		
		
		
		//benzcheck
	}

}
