package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IframeExample2 
{

	public static void main(String[]args) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		
		D.manage().window().maximize();
		
		D.switchTo().frame("courses-iframe");
		
		WebElement NavigationBar=D.findElementByClassName("navbar-toggle");
		NavigationBar.click();
		
		Thread.sleep(5000);
		WebElement LoginLink=D.findElementByPartialLinkText("Login");
		LoginLink.click();
		
		
	}

}
