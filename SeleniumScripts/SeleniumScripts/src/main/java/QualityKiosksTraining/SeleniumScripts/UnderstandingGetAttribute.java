package QualityKiosksTraining.SeleniumScripts;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class UnderstandingGetAttribute {

	public static void main(String[] args) 
	{
		
		System.setProperty("webdriver.chrome.driver", "D:\\QualityKioskTraining\\Drivers\\chromedriver.exe");
		ChromeDriver D=new ChromeDriver();
		D.get("https://letskodeit.teachable.com/p/practice");
		WebElement HideButton=D.findElementById("hide-textbox");
		HideButton.click();
		WebElement NameField=D.findElementByName("show-hide");
		String StyleAttrValue=NameField.getAttribute("style");
		
		if(StyleAttrValue.equals("display: none;"))
			System.out.println("After clicking on Hide button style attribute changed correctly..PASSED");
		else
			System.out.println("After clicking on Hide button style attribute did not changed correctly..FAILED");
		
		WebElement ShowBtn=D.findElementById("show-textbox");
		ShowBtn.click();
		
		StyleAttrValue=NameField.getAttribute("style");
		
		if(StyleAttrValue.equals("display: block;"))
			System.out.println("After clicking on Show button style attribute changed correctly..PASSED");
		else
			System.out.println("After clicking on Show button style attribute did not changed correctly..FAILED");
		
	}

}
