Flat scripting(Without framework)
	1.Tends to have lot of hard coding like URL of application,Path of driver etc.
	2.Hard coding of Element finding stratergy and locators
	3.No reusable code
	4.Lot of technical coding
	5.No testing framework integration to create reports.
	6.Lack of methodical approach
	7.No directory structure created

Framework:
	1.It has disciplined approach for writing scripts
	2.No hard coding
	3.Script writing becomes less technical
	4.Reusability of code
	5.Proper directory structure
		